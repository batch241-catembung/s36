// controllers contains the function and the business logic of our express js application

//meaning all the operation it can do will be placed in this file

//uses the "require" directive to allow access to the "Task" model which allows us to access methods to perform CRUD operations
//allows us to use the contents of the "task.js" file in the "models" folder
const Task = require("../models/task");

//controller function for GETTING ALL THE TASK
//define the functions to be used in the "taskRoutes.js" file and export these functions

/*module.exports.getAllTasks = () =>{

	// the ".then" method is used to wait for the monngoose "find" method to finish before sending the result back to the route and eventually to the client/Postman
	return Task.find({}).then(result => {
		return result;
	})
}*/

module.exports.getAllTasks = () => {

	//The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman
	
	return Task.find({}).then(result => {

		return result;
	
	})


}



module.exports.createTask =(requestBody) =>{

	//create a task object based on the mongoose model "task"
	let newTask = new Task ({
		//sets the "name" property with the value received from the client 
		name: requestBody.name
	})
	// the first parameter will store the result return by the monggose save method
	//second parameter will store the  "error" object
	return newTask.save().then((task,error)=>{

		if (error){
			console.log(error)
			return false
		} else{
			return task
		}
	})

}


module.exports.deleteTask =(taskId)=>{

	return Task.findByIdAndRemove(taskId).then((removedTask, err)=>{

		if (err){
			console.log(err)
			return false
		} else{
			return removedTask
		}

	})


}

module.exports.updateTask = (taskId, reqBody) =>{

	return Task.findById(taskId).then((result, error)=>{

		if (error){
			console.log(error)
			return false
		} 
			result.name = reqBody.name;

			return result.save().then((updatedTask, saveErr)=>{

				if (saveErr){
					console.log(saveErr)
				}else{
					return updatedTask
				}

			})
		

	})

}

module.exports.getOneTask =(taskId) =>{

	return Task.findById(taskId).then((result, error)=>{

		if (error){
			console.log(error)
			return false
		} 
		return result.save().then((result, saveErr)=>{

			if (saveErr){
				console.log(saveErr)
			}else{
				return result;
			}
		})	
	})
}


module.exports.updateStatus = (taskId) =>{

	return Task.findById(taskId).then((result, error)=>{

		if (error){
			console.log(error)
			return false
		} 
			result.status = "complete";

			return result.save().then((updatedTask, saveErr)=>{

				if (saveErr){
					console.log(saveErr)
				}else{
					return updatedTask
				}

			})
		

	})

}