//set up the dependencies
const express = require("express");
const mongoose = require("mongoose");

//allow us to use all the routes defined in "taskRoute,js"
const taskRoutes = require("./routes/taskRoutes")

//server setup
const app = express();
const port = 4000;



app.use(express.json());
app.use(express.urlencoded({extended:true}));

//database connection
//connecting to mongoDB atlas

//removing the unecessary text effecy by the update
mongoose.set('strictQuery', false);

mongoose.connect("mongodb+srv://admin123:admin123@cluster0.ix4q3te.mongodb.net/s36?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console,"connection error"));
db.once("open", ()=> console.log("we are connect to the cloud database"));

//add the task route

app.use("/tasks", taskRoutes);
//https://localhost:



app.listen(port, ()=> console.log(`now listening port ${port}`));