//contains all the endpoints for our application
//we separate the routns such that "index.js" only contains information on the server

const express = require("express");

// create a ruter instance that functions as middleware and routing system 
//allow acces to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

const taskController = require("../controllers/taskController");

//Routes
// the routes are responsible for defifning the URI's taht our client accesses and trhe corresponding controller function that will be used when a route is accessed
//they invole the controller functions from the controller files
//all the business logic is done in the controller

//route to get all the task
router.get("/", (req, res) =>{

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))

})

//route for CREATING A NEW TASK
router.post("/", (req, res)=>{

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));

})

// route for deleting a task
router.delete("/:id", (req, res)=>{
	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController=> res.send(resultFromController))

})

//route for updating a task
router.put("/:id", (req, res)=>{

	taskController.updateTask(req.params.id, req.body).then(resultFromController=> res.send(resultFromController))

})


//activity retrieve 1 document
router.get("/:id", (req, res)=>{
	console.log(req.params)
	taskController.getOneTask(req.params.id).then(resultFromController=> res.send(resultFromController))

})


//activity update status of task
router.put("/:id/complete", (req, res)=>{

	taskController.updateStatus(req.params.id).then(resultFromController=> res.send(resultFromController))

})

//use "module.exports" to export the router object to us in the "index.js"
module.exports = router;














